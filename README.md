# lab4

## Description
This is an education project written in C.

## Installation
To compile each program, simply run one of the following commands:
```sh
make server
make client
```

## Usage

### Run
To run the program, run one of the following commands:
```sh
./server
./client
```

To clean all *.o files, run:
```sh
make clean
```

### Log
You can find log data for server in this file: ./root/server_log.txt.
