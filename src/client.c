#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#include "./socket/socket.h"
#include "./files/files.h"
#include "./formatting/formatting.h"

const short PORT = 3217;
const int RESPONSE_SIZE = 32768;

int main() {
    struct sockaddr_in address;
    set_up_address(&address, INADDR_LOOPBACK, PORT);

    int tcp_socket = socket(PF_INET, SOCK_STREAM, 0);
    if (tcp_socket == -1) {
        printf("\nError creating socket: %s", strerror(errno));
        exit(EXIT_FAILURE);    
    }

    if (connect(tcp_socket, (struct sockaddr *) &address, sizeof(address)) == -1) {
        printf("\nError connecting to server: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf("\nClient has successfully connected");
    printf("\nProcess: \n\tpid: %d\n\tsid: %d", getpid(), getsid(getpid()));

    while (1) {
        printf("\nEnter next string: ");
        char request[256] = "";
        scanf("%255s", request);
    
        int str_length = strlen(request);

        if (request[str_length - 1] == '\n') {
            request[str_length - 1] = '\0';
        }

        send_message(tcp_socket, request, str_length, STDOUT_FILENO);

        char response[RESPONSE_SIZE];
        recieve_message(tcp_socket, response, RESPONSE_SIZE, STDOUT_FILENO);

        if (!should_keep_connection(request)) {
            break;
        }
    }

    close(tcp_socket);
    printf("\nConnection closed");
    exit(EXIT_SUCCESS);
}