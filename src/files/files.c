#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "files.h"

int open_for_writing(const char *filename) {
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (fd == -1) {
        exit(EXIT_FAILURE);
    }
    return fd;
}