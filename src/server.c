
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h> 
#include<sys/wait.h>

#include "./files/files.h"
#include "./daemon/daemon.h"
#include "./socket/socket.h"
#include "./logging/logging.h"
#include "./formatting/formatting.h"

const short QUEUE_LENGTH = 10;
const unsigned short PORT = 3217;
const int REQUEST_SIZE = 1024;
const int RESPONSE_SIZE = 32768;

void format_response(char *request, char *response);

int main() {             
    struct sockaddr_in client; 
    struct sockaddr_in server; 
    int tcp_socket;                                      
    socklen_t sizeof_client;

    pid_t pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    set_up_daemon();

    int log_fd = open_for_writing("server_log.txt");
    write_process_info_to_log(log_fd);

    set_up_address(&server, INADDR_ANY, PORT);

    tcp_socket = socket(PF_INET, SOCK_STREAM, 0);
    if (tcp_socket == -1) {
        exit_with_logging(EXIT_FAILURE, "\nError creating socket", log_fd);
    }

    start_socket(tcp_socket, server, QUEUE_LENGTH, log_fd);
    write_to_log(log_fd, "\nServer is listening");

    while (1) {
        sizeof_client = sizeof(client);

        int peer_socket; 
        if ((peer_socket = accept(tcp_socket, (struct sockaddr *)&client, &sizeof_client)) == -1) {
            close(tcp_socket);
            close(peer_socket);
            exit_with_logging(EXIT_FAILURE, "\nError accepting client", log_fd);
        }

        write_to_log(log_fd, "\nAccepted new client");

        pid = fork();

        if (pid < 0) {
            close(tcp_socket);
            close(peer_socket);
            exit_with_logging(EXIT_FAILURE, "\nError forking", log_fd);
        } else if (pid > 0) {
            // int status;
            // while (wait(&status) > 0);

            close(peer_socket);
        } else {
            close(tcp_socket);
            while (1) { 
                char request_message[REQUEST_SIZE];
                recieve_message(peer_socket, request_message, REQUEST_SIZE, log_fd);

                char response_message[RESPONSE_SIZE];
                format_response(request_message, response_message);

                send_message(peer_socket, response_message, RESPONSE_SIZE, log_fd);

                if(!should_keep_connection(request_message)) {
                    break;
                }
            }
            exit(EXIT_SUCCESS);
        }
    }
}

void format_response(char *request, char *response) {
    time_t raw_time;
    time(&raw_time);
    struct tm cur_time = (* localtime(&raw_time));

    char time_str[256];
    strftime(time_str, 256, "%H:%M:%S", &cur_time);

    int bytes = sprintf(response, "%d-%s: %s", getpid(), time_str, request);
    if (bytes < 0) {
        exit(EXIT_FAILURE);
    };
}