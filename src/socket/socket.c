#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h> 

#include "socket.h"
#include "../logging/logging.h"

void set_up_address(struct sockaddr_in *address, int s_addr, int port) {
    address->sin_family = PF_INET;
    address->sin_addr.s_addr = htonl(s_addr);
    address->sin_port = htons(port);
}

void start_socket(int socket, struct sockaddr_in address, int queue_length, int log_fd) {
    if (bind(socket, (struct sockaddr *) &address, sizeof(address)) == -1) {
        write_to_log(log_fd, "\nError binding socket");
        close_socket(socket, log_fd);
        exit(EXIT_FAILURE);
    }

    if (listen(socket, queue_length) == -1) {
        write_to_log(log_fd, "\nError listening");
        close_socket(socket, log_fd);
        exit(EXIT_FAILURE);
    }
}

void close_socket(int socket, int log_fd) {
    write_to_log(log_fd, "\nClosing connection");

    if (shutdown(socket, SHUT_RDWR) == -1) {
        write_to_log(log_fd, "\nError shutting down the socket");
        exit(EXIT_FAILURE);
    }

    if (close(socket) == -1) {
        write_to_log(log_fd, "\nError closing the socket");
        exit(EXIT_FAILURE);
    }
}

void recieve_message(int socket, char *buffer, int bytes_to_receive, int log_fd) {
    if (recv(socket, buffer, bytes_to_receive, 0) == -1) {
        write_to_log(log_fd, "\nError receiving message from client");
        exit(EXIT_FAILURE);
    }
    write_to_log(log_fd, "\nReceived message: ");
    write_to_log(log_fd, buffer);
    return;
}

void send_message(int socket, char *message, int bytes_to_send, int log_fd) {
    if (send(socket, message, bytes_to_send, 0) == -1) {
        write_to_log(log_fd, "\nError sending message to client");
        exit(EXIT_FAILURE);
    }
    write_to_log(log_fd, "\nMessage sent");
}

bool should_keep_connection(char *request_message) {
    return strcmp("close", request_message) != 0;
}